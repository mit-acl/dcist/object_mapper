/*
Transform & publish messags of type 
	geometry_msgs/PoseArray 
to 
	nav_msgs/Path 
*/
#pragma once

#include <vector>
#include <cmath>
#include <random>
#include <deque>
#include <utility>
#include <cstdint>
#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <assert.h>
#include <pthread.h>

#include <ros/ros.h>
#include <ros/console.h>
#include <ros/time.h>

#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseArray.h>

#include <nav_msgs/Path.h>

namespace mapper 
{

class Converter {
	
	public:
		Converter(ros::NodeHandle nh, ros::NodeHandle nhp);
		~Converter() = default;

	private:
		ros::NodeHandle nh_, nhp_;

		//Subsribers
		ros::Subscriber posearray_sub; // pose array message

		//Publishers
		ros::Publisher path_pub; // path message
		
		//callbacks
		void posearray2path_cb(const geometry_msgs::PoseArrayConstPtr& msg);
};

} // namespace Mapper
