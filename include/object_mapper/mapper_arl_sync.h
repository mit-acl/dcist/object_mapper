#pragma once

#include <vector>
#include <cmath>
#include <random>
#include <deque>
#include <utility>
#include <cstdint>
#include <string>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <assert.h>
#include <pthread.h>

#include <ros/ros.h>
#include <ros/console.h>
#include <ros/time.h>

#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PointStamped.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/MultiArrayDimension.h>

#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <tf2_eigen/tf2_eigen.h>

#include <arl_perception_msgs/BoundingBox.h>
#include <arl_perception_msgs/ClassLabel.h>
#include <arl_perception_msgs/BoundingBoxArray.h>

#include <visualization_msgs/Marker.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>

#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/sync_policies/exact_time.h>

#include <pcl_ros/transforms.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

// #include <clear_messages/LandmarkConstellation.h>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <Eigen/StdVector>


class Mapper {
	
	public:
		Mapper(ros::NodeHandle nh_, ros::NodeHandle nhp_);
		~Mapper() = default;

	private:
		double max_dist = 20; // maximum distance of object from camera to be considered a detected object
		double fusion_radius = 3; // fuse points that are within the fusion_radius proximity
		double min_sightings = 3; // minimum number of times that an object has to be observed to be added to map

		// frame names for camera & map (i.e., reference frame) in the tf tree
		std::string frame_id_camera = "forward_color_optical_frame";
		std::string frame_id_map = "map";

	private:
		ros::NodeHandle nh, nhp;

		//variables from camera info topic
		Eigen::Matrix<float, 3, 4> K;
		Eigen::Matrix<float, 3, 3> K_inv;

		bool camera_info_initialized = false;

		unsigned camera_width;
		unsigned camera_height;

		//variables for projection from 3d points to image points
		Eigen::Vector3f uv;
		Eigen::Vector4f testpoint;

		//Variables for reconstruction
		Eigen::Vector3f boundingbox_centroid;
		Eigen::Vector3f reconstructed_point;
		sensor_msgs::PointCloud lidar_points;

		// landmarks
		static constexpr int DIM = 3; // assuming 3D points
		Eigen::Matrix3Xd landmark_history;	// previousbly seen landmarks (in map frame)
		std::vector<int> detection_counts; // how many frames a landmark was observerd
		bool landmarks_updated = false; // flag to publish landmarks only when they change

		//Subsribers
		ros::Subscriber camera_info_sub;
		message_filters::Subscriber<sensor_msgs::PointCloud2> lidar_points_sub;
		message_filters::Subscriber<arl_perception_msgs::BoundingBoxArray> detections_sub;
		
		//TimeSynchronizer
		typedef message_filters::sync_policies::ApproximateTime<
					arl_perception_msgs::BoundingBoxArray, sensor_msgs::PointCloud2> MySyncPolicy;
		
		typedef message_filters::Synchronizer<MySyncPolicy> Sync;

		boost::shared_ptr<Sync> sync;

		//Publishers
		// ros::Publisher reconstruction_pub;
		// ros::Publisher map_pub;
		ros::Publisher landmarks_pub;


		//tf listener
		tf::TransformListener tf_listener;
		
		//callbacks
		void camera_info_cb(const sensor_msgs::CameraInfoConstPtr& msg);
        // void pose_cb(const nav_msgs::OdometryConstPtr& msg);	
		void alignment_cb(const arl_perception_msgs::BoundingBoxArrayConstPtr& detection_msg,
						  const sensor_msgs::PointCloud2ConstPtr& lidar_points_msg);

};

