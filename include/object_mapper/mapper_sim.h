#pragma once

#include <vector>
#include <cmath>
#include <random>
#include <deque>
#include <utility>
#include <cstdint>
#include <string>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <assert.h>
#include <pthread.h>

#include <ros/ros.h>
#include <ros/console.h>
#include <ros/time.h>

#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PointStamped.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/MultiArrayDimension.h>

#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <tf2_eigen/tf2_eigen.h>

#include <object_mapper/ImageDetections.h>
#include <object_mapper/Detection.h>

#include <visualization_msgs/Marker.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>

#include <pcl_ros/transforms.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

// #include <clear_messages/LandmarkConstellation.h>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <Eigen/StdVector>


class Mapper {
	
	public:
		Mapper(ros::NodeHandle nh_, ros::NodeHandle nhp_);
		~Mapper() = default;

	private:
		double max_dist = 20; // maximum distance of object from camera to be considered a detected object
		double fusion_radius = 3; // fuse points the are within the fusion_radius proximity

		// frame names for camera & map (i.e., reference frame) in the tf tree
		std::string frame_id_camera = "samoyed/camera";
		std::string frame_id_map = "samoyed/map";

	private:
		ros::NodeHandle nh, nhp;

		//variables from camera info topic
		Eigen::Matrix<float, 3, 4> K;
		Eigen::Matrix<float, 3, 3> K_inv;

		bool camera_info_initialized = false;

		unsigned camera_width;
		unsigned camera_height;

		//variables for projection from 3d points to image points
		Eigen::Vector3f uv;
		Eigen::Vector4f testpoint;

		// // robot pose in the world
		// Eigen::Affine3d T_WRi;  // robots initial position w.r.t world
		// Eigen::Affine3d T_WR; // last received (i.e., current) robot poses
		// bool r_first_pose = false;
	
		//Variables for reconstruction
		Eigen::Vector3f boundingbox_centroid;
		Eigen::Vector3f reconstructed_point;
		
		sensor_msgs::PointCloud lidar_points;

		// landmarks
		static constexpr int DIM = 3; // assuming 3D points
		Eigen::Matrix3Xd landmark_history;	// previousbly seen landmarks (in map frame)


		//Subsribers
		ros::Subscriber camera_info_sub;
        // ros::Subscriber pose_sub;
		ros::Subscriber lidar_points_sub;
		ros::Subscriber detections_sub;
		
		//Publishers
		ros::Publisher reconstruction_pub;
		ros::Publisher map_pub;
		ros::Publisher landmarks_pub;
		
		//tf listener
		tf::TransformListener tf_listener;
		
		//callbacks
		void camera_info_cb(const sensor_msgs::CameraInfoConstPtr & msg);
        // void pose_cb(const nav_msgs::OdometryConstPtr& msg);
		void lidar_cb(const sensor_msgs::PointCloud2ConstPtr msg);
		void alignment_cb(const object_mapper::ImageDetectionsConstPtr& detection_msg);	

};

