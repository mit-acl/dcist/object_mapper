#!/usr/bin/env python3
"""Publish gt object points"""
import argparse
import csv
import numpy as np
from sensor_msgs.msg import PointCloud2, PointField
import rospy
import sensor_msgs.point_cloud2 as pc2
import open3d
from scipy.spatial.transform import Rotation as Rot

est_cloud = None
gt_cloud = None

def read_gt_pos(csv_file):
    class_id = []
    pos = []
    with open(csv_file, mode='r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            class_id.append(int(row["class_id"]))
            pos.append([float(row["x"]), float(row["y"]), float(row["z"])])
    return class_id, np.array(pos)

def array_to_pointcloud2(points):
    ros_dtype = PointField.FLOAT32
    dtype = np.float32
    itemsize = np.dtype(dtype).itemsize
    data = points.astype(dtype).tobytes()

    fields = [PointField(
        name=n, offset=i*itemsize, datatype=ros_dtype, count=1)
        for i, n in enumerate('xyz')]

    return PointCloud2(
        height=1,
        width=points.shape[0],
        is_dense=False,
        is_bigendian=False,
        fields=fields,
        point_step=(itemsize * 3),
        row_step=(itemsize * 3 * points.shape[0]),
        data=data
    )

def pointcloud2_to_array(pointcloud):
    points = []
    for point in pc2.read_points(pointcloud, skip_nans=True):
        points.append([point[0], point[1], point[2]])
    return np.array(points)

def est_cloud_cb(pointcloud):
    global est_cloud
    est_cloud = pointcloud2_to_array(pointcloud)

def main():
    """Run some stuff."""
    parser = argparse.ArgumentParser(
        description="publish gt object points from csv."
    )
    parser.add_argument("objects", type=str, help="csv file encoding the objects.")
    parser.add_argument("topic", type=str, help="topic name.")
    parser.add_argument("est_topic", type=str, help="estimated cloud topic.")
    parser.add_argument("frame", type=str, help="frame id.")
    parser.add_argument("--degree", "-d", type=float, default=0.0, help="initial guess yaw. ")
    args = parser.parse_args()

    global gt_cloud
    labels, gt_cloud = read_gt_pos(args.objects)

    pub = rospy.Publisher(args.topic, PointCloud2, queue_size=10)
    sub = rospy.Subscriber(args.est_topic, PointCloud2, est_cloud_cb)

    rospy.init_node("pub_objects_csv")
    r = rospy.Rate(1) # 1hz
    while not rospy.is_shutdown():
        gt_cloud_aligned = None
        if est_cloud is None:
            gt_cloud_aligned = gt_cloud
        else:
            # align gt to est
            print("Aligining gt cloud to est cloud. ")
            threshold = 20
            t_init = np.array([10, 5, 0])
            rot_init = Rot.from_euler('z', args.degree, degrees=True)
            trans_init = np.eye(4)
            # trans_init[:3, :3] = rot_init.as_matrix()
            # trans_init[:3, 3] = t_init
            source = open3d.geometry.PointCloud()
            source.points = open3d.utility.Vector3dVector(est_cloud)
            target = open3d.geometry.PointCloud()
            target.points = open3d.utility.Vector3dVector(gt_cloud)

            reg_p2p = open3d.pipelines.registration.registration_icp(
                source, target, threshold, trans_init,
                open3d.pipelines.registration.TransformationEstimationPointToPoint())
            print(reg_p2p)
            gt_homo_cloud = np.ones([gt_cloud.shape[0], 4])
            gt_homo_cloud[:,:3] = gt_cloud
            gt_cloud_aligned = (np.dot(np.linalg.inv(reg_p2p.transformation), gt_homo_cloud.T)).T[:,:3]
        cloud_msg = array_to_pointcloud2(gt_cloud_aligned)
        cloud_msg.header.frame_id = args.frame
        cloud_msg.header.stamp = rospy.Time.now()
        pub.publish(cloud_msg)
        r.sleep()

if __name__ == "__main__":
    main()
