#!/usr/bin/env python3
"""Generate object mapper input"""
# TODO(yun) make this a node to run online
import rosbag
import argparse
import csv
import numpy as np
from scipy.spatial.transform import Rotation as Rot
from arl_perception_msgs.msg import BoundingBox, BoundingBoxArray
from sensor_msgs.msg import PointCloud2, PointField
import rospy
import sensor_msgs.point_cloud2 as pc2

def pose_matrix(x, y, z, qx, qy, qz, qw):
    return build_pose_matrix(np.array([x, y, z]), np.array([qx, qy, qz, qw]))

def build_pose_matrix(t, q):
    R = Rot.from_quat(q)
    T = np.eye(4)
    T[0:3, 0:3] = R.as_dcm()
    T[0:3, 3] = t
    return T

# TODO(yun) read these from urdf
base_t_ouster = np.array([0.12, 0, 0.315])
base_q_ouster = np.array([0, 0, 0, 1])
base_T_ouster = build_pose_matrix(base_t_ouster, base_q_ouster)

# base_t_forward = np.array([0, -0.059, 0])
# base_q_forward = np.array([0, 0, 0, 1])
# base_T_forward = build_pose_matrix(base_t_forward, base_q_forward)

# forward_t_optical = np.array([0, 0, 0])
# forward_q_optical = np.array([-0.5, 0.5, -0.5, 0.5])
# forward_T_optical = build_pose_matrix(forward_t_optical, forward_q_optical)

# base_T_optical = np.dot(base_T_forward, forward_T_optical)
base_t_optical = np.array([-0.000, -0.059, 0.000])
base_q_optical = np.array([-0.500, 0.498, -0.500, 0.503])
base_T_optical = build_pose_matrix(base_t_optical, base_q_optical)

# 0 0.5 -0.2 1.59 -1.59 0 
rs_t_base = np.array([0, 0.5, -0.2])
rs_q_base = np.array([0.4999078, -0.4999078, 0.5096012, 0.4903988])
rs_T_base = build_pose_matrix(rs_t_base, rs_q_base)

# Camera matrix
K = np.array([[380.82354736328125, 0.0, 325.75006103515625, 0.0], [0.0, 380.3779296875, 245.53099060058594, 0.0], [0.0, 0.0, 1.0, 0.0]])
img_height = 480
img_width = 640

def read_gt_traj(csv_file):
    poses = []
    stamps = []
    with open(csv_file, mode='r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            stamps.append(float(row["timestamp[ns]"]))
            poses.append(np.dot(pose_matrix(row["x"], row["y"], row["z"], 
                row["qx"], row["qy"], row["qz"], row["qw"]), rs_T_base))
    return stamps, poses

def read_gt_pos(csv_file):
    class_id = []
    pos = []
    with open(csv_file, mode='r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            class_id.append(int(row["class_id"]))
            pos.append([float(row["x"]), float(row["y"]), float(row["z"])])
    return class_id, pos

def skew_symm(vector):
    vx = np.zeros([3, 3])
    vx[1, 0] = vector[2]
    vx[0, 1] = -vector[2]
    vx[2, 0] = -vector[1]
    vx[0, 2] = vector[1]
    vx[2, 1] = vector[0]
    vx[1, 2] = -vector[0]
    return vx

def vector_to_rot(vec):
    v = np.cross(np.array([1, 0, 0]), vec)
    s = np.linalg.norm(v)
    c = np.dot(np.array([1, 0, 0]), vec)

    return np.eye(3) + skew_symm(v) +\
        np.dot(skew_symm(v), skew_symm(v)) * (1 - c) / s**2

def objects_seen(pose, object_pos_array, valid_radius, fov):
    # transform objects to camera frame
    homo_pos_array = np.ones([object_pos_array.shape[0], 4])
    homo_pos_array[:,:3] = object_pos_array
    cam_pos_objects = (np.dot(np.linalg.inv(pose), homo_pos_array.T).T)[:, :3]

    # first check that positions are within a valid radius
    dist = np.linalg.norm(cam_pos_objects, axis=1)

    # angle_error = []
    # for obj in cam_pos_objects:
    #     rotation = Rot.from_matrix(vector_to_rot(obj))
    #     angle_error.append(np.linalg.norm(rotation.as_rotvec()) * 180 / np.pi)
    # angle_error = np.array(angle_error)
    return np.where(dist < valid_radius)[0]

def array_to_pointcloud2(points):
    ros_dtype = PointField.FLOAT32
    dtype = np.float32
    itemsize = np.dtype(dtype).itemsize
    data = points.astype(dtype).tobytes()

    fields = [PointField(
        name=n, offset=i*itemsize, datatype=ros_dtype, count=1)
        for i, n in enumerate('xyz')]

    return PointCloud2(
        height=1,
        width=points.shape[0],
        is_dense=False,
        is_bigendian=False,
        fields=fields,
        point_step=(itemsize * 3),
        row_step=(itemsize * 3 * points.shape[0]),
        data=data
    )

def create_lidar_detect_msg(stamp, traj_pose, object_labels, object_posit):
     # transform objects to camera frame
    homo_pos_array = np.ones([len(object_posit), 4])
    homo_pos_array[:,:3] = np.array(object_posit)
    base_pos_objects = np.dot(np.linalg.inv(traj_pose), homo_pos_array.T).T
    cam_pos_objects = np.dot(np.linalg.inv(base_T_optical), base_pos_objects.T).T
    centroid = np.dot(K, cam_pos_objects.T).T
    centroid = (centroid.T / centroid[:,2]).T
    lidar_pos_objects = np.dot(np.linalg.inv(base_T_ouster), base_pos_objects.T).T[:,:3]

    bb_array_msg = BoundingBoxArray()

    # for now we don't care too much about the actual size of the bounding box
    for i in range(len(object_labels)):
        if centroid[i, 0] < img_width and centroid[i, 1] < img_height \
            and centroid[i, 0] > 0 and centroid[i, 1] > 0 and cam_pos_objects[i, 2] > 0:
            bb = BoundingBox()
            bb.probability = 1.0
            bb.xmin = max(0, int(centroid[i, 0]) - 50)
            bb.ymin = max(0, int(centroid[i, 1]) - 50)
            bb.xmax = min(img_width, int(centroid[i, 0]) + 50)
            bb.ymax = min(img_height, int(centroid[i, 1]) + 50)
            bb.class_id = object_labels[i]
            bb_array_msg.bounding_boxes.append(bb)
            print(lidar_pos_objects[i,:])
            print(cam_pos_objects[i,:])
            print(centroid[i,:])
            # print(bb)
            # homo_lp = np.ones(4)
            # homo_lp[:3] = lidar_pos_objects[i,:]
            # testpoint = np.matmul(K, homo_lp)
            # print(testpoint / testpoint[2])
    lidar_msg = array_to_pointcloud2(lidar_pos_objects)
    print("---")
    return array_to_pointcloud2(lidar_pos_objects), bb_array_msg


def main():
    """Run some stuff."""
    parser = argparse.ArgumentParser(
        description="utiltiy to generate simulated object mapper inputs (detection + lidar detections)."
    )
    parser.add_argument("gt_traj", type=str, help="csv file encoding the gt trajectory.")
    parser.add_argument("objects", type=str, help="csv file encoding the objects.")
    parser.add_argument("robot", type=str, help="robot name.")
    parser.add_argument("output", type=str, help="bag to write to. ")
    parser.add_argument("--radius", "-r", type=float, default=20.0, help="effective radius where an object can be seen from a pose. ")
    parser.add_argument("--fov", "-f", type=float, default=180.0, help="field of view where an object can be seen from a pose (deg). ")
    parser.add_argument("--time_offset", "-d", type=float, default=0.0, help="time offset for bag time and stamps.")
    args = parser.parse_args()

    detection_topic = "/{}/detection_out".format(args.robot)
    lidar_topic ="/{}/object_points".format(args.robot)

    camera_frame = "{}/forward_color_optical_frame".format(args.robot)
    lidar_frame = "{}/ouster_link".format(args.robot)

    stamps, traj = read_gt_traj(args.gt_traj)
    labels, obj_pos = read_gt_pos(args.objects)

    outbag = rosbag.Bag(args.output, "w")

    for index in range(len(traj)):
        seen_indices = objects_seen(traj[index], np.array(obj_pos), args.radius, args.fov)
        if len(seen_indices) == 0: 
            continue
        lidar_msg, detect_msg = create_lidar_detect_msg(stamps[index], traj[index],
            [labels[i] for i in seen_indices], [obj_pos[i] for i in seen_indices])
        if (len(detect_msg.bounding_boxes) == 0):
            continue
        ros_time = rospy.Time(float(stamps[index]) * 1e-9 + args.time_offset)
        lidar_msg.header.frame_id = lidar_frame
        detect_msg.header.frame_id = camera_frame
        lidar_msg.header.stamp = ros_time
        detect_msg.header.stamp = ros_time
        outbag.write(lidar_topic, lidar_msg, ros_time)
        outbag.write(detection_topic, detect_msg, ros_time)
    outbag.close()

if __name__ == "__main__":
    main()
