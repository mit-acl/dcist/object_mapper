#include "object_mapper/mapper.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <cassert>

#include <sensor_msgs/point_cloud_conversion.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <eigen_conversions/eigen_msg.h>

namespace mapper
{

Object::Object() {}

DetectionFrame::DetectionFrame() {}


} // namespace mapper




Mapper::Mapper(ros::NodeHandle nh_, ros::NodeHandle nhp_) 
: nh(nh_), nhp(nhp_)
// , sync(detections_sub, lidar_points_sub)
{
	// get parameters
	nhp.getParam("max_dist", max_dist);
	ROS_INFO_STREAM("max_dist: " << max_dist);
	nhp.getParam("fusion_radius", fusion_radius); 
	ROS_INFO_STREAM("fusion_radius: " << fusion_radius);
	nhp.getParam("min_sightings", min_sightings); 
	ROS_INFO_STREAM("min_sightings: " << min_sightings);


	nhp.getParam("frame_id_camera", frame_id_camera); 
	ROS_INFO_STREAM("frame_id_camera: " << frame_id_camera);
	nhp.getParam("frame_id_map", frame_id_map); 
	ROS_INFO_STREAM("frame_id_map: " << frame_id_map);
	nhp.getParam("frame_id_base", frame_id_base); 
	ROS_INFO_STREAM("frame_id_base: " << frame_id_base);

	//initialize subscribers
	camera_info_sub = nh.subscribe("/robot/camera_info", 1, &Mapper::camera_info_cb, this);    

	detections_sub.subscribe(nh, "/robot/detections", 1);
	lidar_points_sub.subscribe(nh, "/robot/lidar_points", 1);
	
	sync.reset(new Sync(MySyncPolicy(1000), detections_sub, lidar_points_sub));
	sync->registerCallback(boost::bind(&Mapper::reconstruction_cb, this, _1, _2));

	//initialize publishers
    // reconstruction_pub = nh.advertise<sensor_msgs::PointCloud2>("landmarks_odom_frame",1);
	// map_pub = nh.advertise<sensor_msgs::PointCloud2>("landmarks_map_frame", 1);
	landmarks_pub = nh.advertise<sensor_msgs::PointCloud2>("landmarks",1);

	traj_sub = nh.subscribe("/robot/trajectory", 1, &Mapper::traj_cb, this);

	ROS_INFO_STREAM("Mapper construction finished.");
}
		
// ----------------------------------------------------------------------------
// ROS Callbacks
// ----------------------------------------------------------------------------
void Mapper::camera_info_cb(const sensor_msgs::CameraInfoConstPtr& msg) {
	//do this once
	if(!camera_info_initialized)
    {
		//Define K matrix
		for(int i = 0;i < 3;i++){
			for(int j = 0;j < 4;j++){
				K(i, j) = msg->P[j+i*4];
			}
		}
		camera_width = msg->width;
		camera_height = msg->height;
		//Get inverse of K matrix
		K_inv = ((K.block(0,0,3,3)).inverse());

        // frame_id_camera = msg->header.frame_id;

		camera_info_initialized = true;
	}
	// ROS_INFO_STREAM("got cam info");
	// ROS_INFO_STREAM("K: \n" << K);
}


// ----------------------------------------------------------------------------
void Mapper::reconstruction_cb(const arl_perception_msgs::BoundingBoxArrayConstPtr& detection_msg,
						 	   const sensor_msgs::PointCloud2ConstPtr& lidar_points_msg) 
{
	// ROS_INFO_STREAM("started reconstruction");

	// vector to contain object detections
	std::vector<arl_perception_msgs::BoundingBox> object_vector;

	// add objects to object_vector
	for(int i = 0; i< detection_msg->bounding_boxes.size(); ++i) {
		arl_perception_msgs::BoundingBox object = detection_msg->bounding_boxes[i];        
		
		// ROS_WARN_STREAM("object class: " << object.class_id);
		// ROS_WARN_STREAM("object_bb: " << object.ymax << ", "
		// 								 << object.ymin << ", "
		// 							 	 << object.xmin << ", "
		// 							  	 << object.xmax);		

		// use only certain objects: 
		if (object.class_id==5) 
		{
			// ROS_WARN_STREAM("object class: " << object.class_id);
			object_vector.push_back(object);
		} 	
	} 			

	// ROS_WARN_STREAM("object_vector.size: " << object_vector.size());

	if (object_vector.size() == 0) {
		return;
	}

	// TODO: convert to PointCloud2
	// see:
	// http://docs.ros.org/en/kinetic/api/pcl_ros/html/namespacepcl__ros.html#aad1ce4ad90ab784aae6158419ad54d5f
	// http://wiki.ros.org/tf/Tutorials/Writing%20a%20tf%20listener%20%28C%2B%2B%29
	//

	// create an output pointcloud to contain the conversion from the lidar points pointcloud2
	// sensor_msgs::PointCloud lidar_points;
	sensor_msgs::PointCloud lidar_points;
	try {
		sensor_msgs::convertPointCloud2ToPointCloud(*lidar_points_msg, lidar_points);
	}catch(std::exception &e) {
		ROS_ERROR("Lidar Points Runtime Error: %s", e.what());
	}
	
	// ROS_WARN_STREAM("got lidar");

	// create a new lidar_points pointcloud in the camera frame
	sensor_msgs::PointCloud lidar_points_camera_frame;

	// transform the lidar_points into the camera frame	
	try {
        tf_listener.transformPointCloud(frame_id_camera, lidar_points, lidar_points_camera_frame);
    } catch (tf::TransformException &ex) {
        ROS_WARN_STREAM("Exception trying to transform pointcloud to camera frame: " << ex.what());
    }
	// ROS_INFO("transformed lidar frame. lidar cloud has %d points. ", lidar_points_camera_frame.points.size());
	if (lidar_points_camera_frame.points.size() == 0) {
		ROS_WARN_STREAM("No lidar points in camera frame.");
		return;
	}

	// distance of objects from the camera
	std::vector<std::vector<float>> distances(object_vector.size());
	
	for(int i = 0; i < lidar_points_camera_frame.points.size(); ++i){
		
		// remove points behind camera
		if(lidar_points_camera_frame.points[i].z < 0) continue;
		
		// update the testpoint 
		testpoint(0) = lidar_points_camera_frame.points[i].x;
		testpoint(1) = lidar_points_camera_frame.points[i].y;
		testpoint(2) = lidar_points_camera_frame.points[i].z;
		testpoint(3) = 1.0;
		
		// TODO: use OpenCV project points:
		// https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html#projectpoints

		// project point
		uv = K * testpoint;		
		
		// recover projected point
		float u = uv(0) / uv(2);
		float v = uv(1) / uv(2);

		// ROS_WARN_STREAM("u: " << u << "v: " << v);
		
		// go through object_vector and add distances from camera to lidar points inside each object's bounding box
		for(int j=0; j<object_vector.size(); ++j) 
		{						
			if(v < object_vector[j].ymax && v > object_vector[j].ymin && u > object_vector[j].xmin && u < object_vector[j].xmax ) 
			{
				distances[j].push_back(testpoint.head(3).norm());
				// ROS_WARN_STREAM("added lidar point for object " << j);
				// ROS_WARN_STREAM("testpoint: \n " << testpoint);
				// ROS_WARN_STREAM("testpoint.head(3).norm(): " << testpoint.head(3).norm());
			}			
		}		
	}	
	// ROS_INFO_STREAM("distances in BB found");


	// flag for objects with valid reconstruction
	std::vector<bool> obj_is_valid(object_vector.size(), true);

	// create a pointcloud to contain the reconstructed landmarks in the camera frame
	sensor_msgs::PointCloud lidar_camera_reconstructed_landmarks;
	lidar_camera_reconstructed_landmarks.header.frame_id = frame_id_camera;
	

	for(int i=0; i < object_vector.size(); ++i) 
	{
		//create centroid point
		float u_centroid = object_vector[i].xmin + ((object_vector[i].xmax - object_vector[i].xmin)/2);
		float v_centroid = object_vector[i].ymax + ((object_vector[i].ymin - object_vector[i].ymax)/2);
		Eigen::Vector3f boundingbox_centroid;
		boundingbox_centroid = Eigen::Vector3f(u_centroid, v_centroid, 1.0);

		// ROS_INFO_STREAM("boundingbox_centroid: " << boundingbox_centroid);
		
		//variable for resultant reconstructed distance
		float distance;		

		// skip iteration if no distance info
		if (distances[i].size() == 0) 
		{
			obj_is_valid[i] = false;
			continue;				 
		}

		// sort distances to get the median easily
		std::sort(distances[i].begin(), distances[i].end());
		if(distances[i].size() % 2 == 0) //median distance if even number of points
		{			
			distance = (distances[i][(distances[i].size()/2)-1] + distances[i][distances[i].size()/2])/2;
		} 
		else //median distance if odd number of points
		{			
			distance = (distances[i][(distances[i].size() - 1)/2]);
		}		

		// if object is too far 
		if (distance > max_dist) 
		{
			obj_is_valid[i] = false;
			// ROS_WARN_STREAM("distance: " << distance);
			// continue;				 
		} 
		else
		{
			// ROS_INFO_STREAM("distance: " << distance << ", type: " << object_vector[i].class_id << ";   ");
		}

		// reconstruct 3D point
		Eigen::Vector3f reconstructed_point;
		reconstructed_point = K_inv * boundingbox_centroid;
		
		// normalize reconstructed point
		reconstructed_point = reconstructed_point/(reconstructed_point.norm());

		// use the median distance
		reconstructed_point = reconstructed_point*distance;
	
		// convert eigen vector to geometry_msgs::Point32
		geometry_msgs::Point32 pc_reconstructed_point;

		pc_reconstructed_point.x = reconstructed_point(0);
		pc_reconstructed_point.y = reconstructed_point(1);
		pc_reconstructed_point.z = reconstructed_point(2);
		
		// add recontructed point to point cloud            
		lidar_camera_reconstructed_landmarks.points.push_back(pc_reconstructed_point);			
	}
	
	// ROS_WARN_STREAM("object point cloud constructed");

	if (lidar_camera_reconstructed_landmarks.points.size() == 0) {
		return; // nothing to publish
	}

	// bring points to base frame
	sensor_msgs::PointCloud lidar_camera_reconstructed_landmarks_base_frame; // landmarks in the map frame (i.e., start frame)
	try {
        tf_listener.transformPointCloud(frame_id_base, lidar_camera_reconstructed_landmarks, lidar_camera_reconstructed_landmarks_base_frame);
    } catch (tf::TransformException &ex) {
        ROS_WARN_STREAM("Exception trying to transform point cloud to base frame: " << ex.what());
		return;
    }	
	
	// store objects
	mapper::DetectionFrame detection_frame;
	detection_frame.timestamp = lidar_points.header.stamp.toSec();

	for (int i=0; i<object_vector.size(); ++i)
	{
		if (obj_is_valid[i])
		{
			mapper::Object object;

			object.type = (uint) object_vector[i].class_id;
			object.confidence = (float) object_vector[i].probability;

			object.pose_local = Eigen::Vector3d(lidar_camera_reconstructed_landmarks_base_frame.points[i].x,
												lidar_camera_reconstructed_landmarks_base_frame.points[i].y,
												lidar_camera_reconstructed_landmarks_base_frame.points[i].z);
			
			// ROS_WARN_STREAM("object.pose_local: \n" << object.pose_local);

			detection_frame.objects.push_back(object);
		}		
	}

	// ROS_ERROR_STREAM("detection_frame created: ");
	// ROS_WARN_STREAM("timestamp:" << detection_frame.timestamp);
	// for (int i=0; i<detection_frame.objects.size(); ++i)
	// {
	// 	ROS_WARN_STREAM("type: \n" << detection_frame.objects[i].type);
	// 	ROS_WARN_STREAM("confidence: \n" << detection_frame.objects[i].confidence);
	// 	ROS_WARN_STREAM("pose_local: \n" << detection_frame.objects[i].pose_local);
	// }
    
	// store detection frame
	detection_frames.push_back(detection_frame);

	// ROS_WARN_STREAM("number of frames:" << detection_frames.size());
}




// ----------------------------------------------------------------------------
void Mapper::traj_cb(const nav_msgs::PathConstPtr& msg)
{	
	// ROS_INFO_STREAM("Object mapping started");

	// if detection_frames is empty, return
	if (detection_frames.empty())
	{
		return;
	}

	// ------------------------------------------
	// get robot's trajectory
	nav_msgs::Path trajectory = *msg; // robot trajectory
	
	// extract timestamp of all pose messages in trajectory
	std::vector<double> traj_timestamps(trajectory.poses.size());
	for (int i=0; i<trajectory.poses.size(); ++i)
	{
		traj_timestamps[i] = trajectory.poses[i].header.stamp.toSec();

		// ROS_INFO_STREAM("timestamp " << i << ":\n" << trajectory.poses[i].header.stamp.toSec());
		// ROS_INFO_STREAM("pose " << i << ":\n" << trajectory.poses[i].pose);
	}

	// std::cout << "traj_timestamps: \n" << std::endl;
	// for (double& elem : traj_timestamps)
	// 	{
	// 		std::cout << elem <<", ";
	// 	}
	// std::cout << std::endl;

	// ------------------------------------------
	// loop on detection frame (in detection_frames)
	for (int i=0; i<detection_frames.size(); ++i)
	{
		// ROS_ERROR_STREAM("detection frame: " << i);

		// get frame and its timestamp
		mapper::DetectionFrame& frame = detection_frames[i];
		double& frame_timestamp = frame.timestamp;

		// ROS_WARN_STREAM("frame timestamp " << i << ":\n" << frame_timestamp);

		// find index of closest pose message using the timestamps
		// subtract frame_timestamp from all elements of traj_timestamps and find absolute value
		// see: https://stackoverflow.com/questions/4461446/stl-way-to-add-a-constant-value-to-a-stdvector
		std::vector<double> diff_timestamps;
		diff_timestamps = traj_timestamps;
		for (double& elem : diff_timestamps)
		{
			elem = std::abs(elem-frame_timestamp);
			// std::cout << elem <<", ";
		}
		// std::cout << std::endl;

		// find closest timestamp as the minimum of diff_timestamps
		auto min_elem_ptr = std::min_element(diff_timestamps.begin(), diff_timestamps.end());
		double min_elem = *min_elem_ptr;
		int min_index = min_elem_ptr - diff_timestamps.begin();

		// ROS_WARN_STREAM("min_index: " << min_index);
						  
		// if min_time_diff > thresh, then give warning and skip
		if (min_elem > max_time_diff)
		{
			// ROS_WARN_STREAM("No pose in robot trajectory is close to detection frame timestamp.");
			continue; // skip this iteration
		}

		// ROS_WARN_STREAM("min_elem: " << min_elem);

		// transform pose_local of objects in the frame to pose_global
		// get pose of the robot in global frame (cast to eigen)
		Eigen::Affine3d T_rw; // pose of robot in world frame
		tf::poseMsgToEigen(trajectory.poses[min_index].pose, T_rw);

		// ROS_WARN_STREAM("T_rw: \n" << T_rw.matrix());

		for (int j=0; j<frame.objects.size(); ++j) // for all objects
		{
			mapper::Object& object = frame.objects[j];
			// transform local pose to global pose
			object.pose_global = T_rw * object.pose_local;

			// ROS_WARN_STREAM("frame " << i << " object " << j);
			// ROS_WARN_STREAM("pose_local is: \n" << object.pose_local << "\n pose_global is: \n" << object.pose_global);
		}

	
	}
		

	// ------------------------------------------
	// fuse landmarks from t=0 till t=now based on position	
	// TODO: use CLEAR for fusion (later switch to MIXER)\
	// TODO: use landmark type & confidence in fusion

	/*
	// keep all landmarks
	Eigen::Matrix3Xd landmark_history;	// all landmarks (in global frame)	
	for (int i=0; i<detection_frames.size(); ++i)
	{
		mapper::DetectionFrame& frame = detection_frames[i];

		for (int j=0; j<frame.objects.size(); ++j) // for all objects in a frame
		{
			mapper::Object& object = frame.objects[j];

			landmark_history.conservativeResize(landmark_history.rows(), landmark_history.cols()+1);
			landmark_history.col(landmark_history.cols()-1) = object.pose_global;
			// detection_counts.push_back(1); // add detect count of 1 for the new landmark
		}
	}
	*/

	// fuse landmarks frame by frame	
	Eigen::Matrix3Xd landmarks; // all landmarks (in global frame)
	std::vector<int> landmarks_views; // number of observations of each landmark
	std::vector<uint> landmarks_type; // type of each landmark
	for (const mapper::DetectionFrame& frame : detection_frames) // loop over all frames
	{
		int landmarks_count = landmarks.cols(); // number of landmarks from t=0 to t=now		
		// ROS_WARN_STREAM("landmarks_count: " << landmarks_count);

		if (landmarks_count==0) // for first frame, simply add all landmakrs
		{
			for (const mapper::Object& object : frame.objects) // loop over all objects in the frame
			{				
				// add new landmark
				landmarks.conservativeResize(landmarks.rows(), landmarks.cols()+1);
				landmarks.col(landmarks.cols()-1) = object.pose_global;
				landmarks_views.push_back(1); // observation count of landmark
				landmarks_type.push_back(object.type); // type of landmark
			}

			continue; // skip the rest & go to the next frame
		} 

		// fuse new observations with previous landmarks, or add as new landmarks
		for (const mapper::Object& object : frame.objects) // loop over all objects in the frame
		{
			const Eigen::Vector3d& p = object.pose_global; // object position (in global coords)
			const uint& p_type = object.type; // landmark type
			// ROS_WARN_STREAM("p:\n" << p);
			
			// distance of landmark (in this frame) to all prior landmarks
			std::vector<double> distvec(landmarks_count);
			for (size_t j=0; j<landmarks_count; ++j) 
			{
				const Eigen::Vector3d& q = landmarks.col(j);
				Eigen::Vector3d diff = p - q;
				distvec[j] = diff.norm();
				// ROS_WARN_STREAM("norm: " << distvec[j]);				 												
			}
			
			// closest existing landmark to current landmark
			auto min_elem_ptr = std::min_element(distvec.begin(), distvec.end());
			double min_elem = *min_elem_ptr; // distance to closet landmark
			int min_index = min_elem_ptr - distvec.begin();
			const Eigen::Vector3d& q = landmarks.col(min_index); // closest existing landmark
			const uint& q_type = landmarks_type[min_index]; // landmark type

			if ((min_elem < fusion_radius) && (q_type == p_type)) // fuse landmark points if they have same type and closer than threshold distance
			{				
				int& q_views = landmarks_views[min_index]; // (reference to) number of views so far
				landmarks.col(min_index) = (p + q_views * q) / (q_views+1); // average position of all observations
				q_views ++; // increase view count by 1
				// ROS_INFO_STREAM("updated landmark");
			}
			else // add as a new landmark 
			{
				landmarks.conservativeResize(landmarks.rows(), landmarks.cols()+1);
				landmarks.col(landmarks.cols()-1) = p;
				landmarks_views.push_back(1); // add detect count of 1 for the new landmark
				landmarks_type.push_back(p_type); // store landmark type
				// ROS_INFO_STREAM("added new landmark");
			}
		}


	}


	// ------------------------------------------
	// publish landmarks 

	// create point cloud object for landmarks that are detected > min_sightings
	pcl::PointCloud<pcl::PointXYZ> pt_cloud_pcl;
	for (size_t i=0; i < landmarks.cols(); ++i) 
	{
		// ROS_INFO_STREAM("detection_counts[" << i << "]: " << detection_counts[i]);
		// if (detection_counts[i] >= min_sightings) // landmarks that are detected > min_sightings
		// {
			pcl::PointXYZ point;
			point.x = landmarks(0,i);
			point.y = landmarks(1,i);
			point.z = landmarks(2,i);

			pt_cloud_pcl.push_back(point);
			// ROS_INFO_STREAM("point: " << point);
		// }
	}

	// Convert to ROS PointCloud2 data type
	pcl::PCLPointCloud2 ptcloud2;
	pcl::toPCLPointCloud2(pt_cloud_pcl, ptcloud2);
	sensor_msgs::PointCloud2 msg_pt2_landmarks;	
	pcl_conversions::fromPCL(ptcloud2, msg_pt2_landmarks);
	msg_pt2_landmarks.header.frame_id = frame_id_map;
	msg_pt2_landmarks.header.stamp = ros::Time::now();
	
	// publish landmakrs
	landmarks_pub.publish(msg_pt2_landmarks);

	// ROS_WARN_STREAM("landmarks published.");
	// ROS_WARN_STREAM("Landmarks:");
	// ROS_WARN_STREAM("\n" << landmarks);
}




// ----------------------------------------------------------------------------
int main(int argc, char **argv) 
{
	ros::init(argc, argv, "mapper_arl");
	ros::NodeHandle nh("");
    ros::NodeHandle nhp("~");
	Mapper mapper(nh, nhp);
	ros::spin();
	return 0;
}
