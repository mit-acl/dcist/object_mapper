#include "object_mapper/posearray2path.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <cassert>


// for posearray to path see:
// http://ein57ein.github.io/ros_plugin_demo/pose__array__from__path_8cpp_source.html


namespace mapper
{

Converter::Converter(ros::NodeHandle nh, ros::NodeHandle nhp) 
: nh_(nh), nhp_(nhp)
{
	//initialize subscribers
	posearray_sub = nh_.subscribe("/robot/trajectory", 1, &Converter::posearray2path_cb, this);    
	
	//initialize publishers
	path_pub = nh_.advertise<nav_msgs::Path>("path",1);

	ROS_INFO_STREAM("PoseArray 2 Path converter construction finished.");
}
		
// ----------------------------------------------------------------------------
// ROS Callbacks
// ----------------------------------------------------------------------------
void Converter::posearray2path_cb(const geometry_msgs::PoseArrayConstPtr& msg)
{	
	// ROS_INFO_STREAM("convertor started");

	// get robot's trajectory
	geometry_msgs::PoseArray trajectory = *msg; // robot trajectory
	
	// path message
	nav_msgs::Path path;

	// port posearray to path message
	path.header = trajectory.header;
	


	/*

	path.poses = trajectory.poses;



	// extract timestamp of all pose messages in trajectory
	std::vector<double> traj_timestamps(trajectory.poses.size());
	for (int i=0; i<trajectory.poses.size(); ++i)
	{
		traj_timestamps[i] = trajectory.poses[i].header.stamp.toSec();

		// ROS_INFO_STREAM("timestamp " << i << ":\n" << trajectory.poses[i].header.stamp.toSec());
		// ROS_INFO_STREAM("pose " << i << ":\n" << trajectory.poses[i].pose);
	}
	*/

	// std::cout << "traj_timestamps: \n" << std::endl;
	// for (double& elem : traj_timestamps)
	// 	{
	// 		std::cout << elem <<", ";
	// 	}
	// std::cout << std::endl;
	
}



} // namespace mapper



// ----------------------------------------------------------------------------
// ROS node
// ----------------------------------------------------------------------------
int main(int argc, char **argv)
{
    ros::init(argc, argv, "converter");
    ros::NodeHandle nhtopics("");
    ros::NodeHandle nhparams("~");

    mapper::Converter converter(nhtopics, nhparams);

    ros::spin();
    return 0;
}

