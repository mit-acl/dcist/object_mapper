#include "object_mapper/mapper_arl_sync.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <cassert>

#include <sensor_msgs/point_cloud_conversion.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>


Mapper::Mapper(ros::NodeHandle nh_, ros::NodeHandle nhp_) 
: nh(nh_), nhp(nhp_)
// , sync(detections_sub, lidar_points_sub)
{
	// get parameters
	nhp.getParam("max_dist", max_dist);
	ROS_INFO_STREAM("max_dist: " << max_dist);
	nhp.getParam("fusion_radius", fusion_radius); 
	ROS_INFO_STREAM("fusion_radius: " << fusion_radius);
	nhp.getParam("min_sightings", min_sightings); 
	ROS_INFO_STREAM("min_sightings: " << min_sightings);


	nhp.getParam("frame_id_camera", frame_id_camera); 
	ROS_INFO_STREAM("frame_id_camera: " << frame_id_camera);
	nhp.getParam("frame_id_map", frame_id_map); 
	ROS_INFO_STREAM("frame_id_map: " << frame_id_map);

	//initialize subscribers
	camera_info_sub = nh.subscribe("/robot/camera_info", 1, &Mapper::camera_info_cb, this);
    // pose_sub = nh.subscribe("/robot/odometry", 1, &Mapper::pose_cb, this);
	// camera_info_sub = nh.subscribe("/robot/lidar_points", 1, &Mapper::lidar_cb, this);
	// detections_sub = nh.subscribe("/robot/detections", 1, &Mapper::alignment_cb, this);	    

	detections_sub.subscribe(nh, "/robot/detections", 1);
	lidar_points_sub.subscribe(nh, "/robot/lidar_points", 1);
	
	sync.reset(new Sync(MySyncPolicy(1000), detections_sub, lidar_points_sub));
	sync->registerCallback(boost::bind(&Mapper::alignment_cb, this, _1, _2));

	//initialize publishers
    // reconstruction_pub = nh.advertise<sensor_msgs::PointCloud2>("landmarks_odom_frame",1);
	// map_pub = nh.advertise<sensor_msgs::PointCloud2>("landmarks_map_frame", 1);
	landmarks_pub = nh.advertise<sensor_msgs::PointCloud2>("landmarks",1);


	ROS_INFO_STREAM("Object mapper construction finished.");
}
		
// ----------------------------------------------------------------------------
// ROS Callbacks
// ----------------------------------------------------------------------------

void Mapper::camera_info_cb(const sensor_msgs::CameraInfoConstPtr& msg) {
	//do this once
	if(!camera_info_initialized)
    {
		//Define K matrix
		for(int i = 0;i < 3;i++){
			for(int j = 0;j < 4;j++){
				K(i, j) = msg->P[j+i*4];
			}
		}
		camera_width = msg->width;
		camera_height = msg->height;
		//Get inverse of K matrix
		K_inv = ((K.block(0,0,3,3)).inverse());

        // frame_id_camera = msg->header.frame_id;

		camera_info_initialized = true;
	}
	// ROS_INFO_STREAM("got cam info");
	// ROS_INFO_STREAM("K: \n" << K);
}

// ----------------------------------------------------------------------------
// void Mapper::pose_cb(const nav_msgs::OdometryConstPtr& msg)
// {
//   tf2::fromMsg(msg->pose.pose, T_WR);

//   if (!r_first_pose) {
//     tf2::fromMsg(msg->pose.pose, T_WRi);
//     r_first_pose = true;
//   }

// //   ROS_INFO_STREAM("got pose");
// }

// ----------------------------------------------------------------------------

// void Mapper::lidar_cb(const sensor_msgs::PointCloud2ConstPtr& msg)
// {
// 	// // create an output pointcloud to contain the conversion from the lidar points pointcloud2
// 	// sensor_msgs::PointCloud lidar_points;
// 	try {
// 		sensor_msgs::convertPointCloud2ToPointCloud(*msg, lidar_points);
// 	}catch(std::exception & e) {
// 		ROS_ERROR("Lidar Points Runtime Error: %s", e.what());
// 	}
// //   ROS_INFO_STREAM("got lidar");
// }

///////////////////////////////////////////////////////////////////////////////
// 
// Lidar-camera alignment and object map construnction
// 
///////////////////////////////////////////////////////////////////////////////

void Mapper::alignment_cb(const arl_perception_msgs::BoundingBoxArrayConstPtr& detection_msg,
						  const sensor_msgs::PointCloud2ConstPtr& lidar_points_msg)  						  
{
	// ROS_INFO_STREAM("started alignment");
	landmarks_updated = false; // flag to publish landmarks only when they change	

	// vector to contain object detections
	std::vector<arl_perception_msgs::BoundingBox> object_vector;

	// add objects to object_vector
	for(int i = 0; i< detection_msg->bounding_boxes.size(); ++i) {
		arl_perception_msgs::BoundingBox object = detection_msg->bounding_boxes[i];        
		/* 
        class_id:
        0: background
        1: pelican_case
        2: gas_can
        3: barrier
        4: people
        5: barrel
        6: gas_pump
        7: window
        8: gate
        9: chair
        10: weapons
        11: door_ground
        12: ingress_ground
        13: robot
        14: tires
        15: produce
        16: banana
        17: bike
        18: vehicle
        */

		// ROS_WARN_STREAM("object class: " << object.class_id);
		// ROS_WARN_STREAM("object_bb: " << object.ymax << ", "
		// 							  << object.ymin << ", "
		// 							  << object.xmin << ", "
		// 							  << object.xmax);

		// use only certain objects: 
		if (object.class_id==5) // for MaskRcnn 
		// if (object.class_id==39) // for Yolo (39=barrel, 44=window)
		{
			// ROS_WARN_STREAM("object class: " << object.class_id);
			object_vector.push_back(object);
		} 	
	} 			

	// ROS_WARN_STREAM("object_vector.size: " << object_vector.size());

	if (object_vector.size() == 0) {
		// return;
	}

	// create an output pointcloud to contain the conversion from the lidar points pointcloud2
	// sensor_msgs::PointCloud lidar_points;
	try {
		sensor_msgs::convertPointCloud2ToPointCloud(*lidar_points_msg, lidar_points);
	}catch(std::exception &e) {
		ROS_ERROR("Lidar Points Runtime Error: %s", e.what());
	}
	
	// ROS_WARN_STREAM("got lidar");


	// create a new lidar_points pointcloud in the camera frame
	sensor_msgs::PointCloud lidar_points_camera_frame;

	// transform the lidar_points into the camera frame
	try {
        tf_listener.transformPointCloud(frame_id_camera, lidar_points, lidar_points_camera_frame);
    } catch (tf::TransformException &ex) {
        ROS_WARN_STREAM("Exception trying to transform pointcloud: " << ex.what());
    }
	// ROS_INFO("transformed lidar frame. lidar cloud has %d points. ", lidar_points_camera_frame.points.size());
	if (lidar_points_camera_frame.points.size() == 0) {
		ROS_WARN_STREAM("No lidar points in camera frame.");
		return;
	}

	// distance of objects from the camera
	std::vector<std::vector<float>> distances(object_vector.size());
	
	for(int i = 0; i < lidar_points_camera_frame.points.size(); ++i){
		
		// get rid of points behind the camera
		if(lidar_points_camera_frame.points[i].z < 0) continue;
		
		// update the testpoint 
		testpoint(0) = lidar_points_camera_frame.points[i].x;
		testpoint(1) = lidar_points_camera_frame.points[i].y;
		testpoint(2) = lidar_points_camera_frame.points[i].z;
		testpoint(3) = 1.0;
		
		// project point
		uv = K*testpoint;		
		
		// recover projected point
		float u = uv(0)/uv(2);
		float v = uv(1)/uv(2);

		// ROS_WARN_STREAM("u: " << u << "v: " << v);
		
		// go through object_vector and add distances from camera to lidar points inside each object's bounding box
		for(int j=0; j<object_vector.size(); ++j) {						
			if(v < object_vector[j].ymax && v > object_vector[j].ymin && u > object_vector[j].xmin && u < object_vector[j].xmax ) {
				distances[j].push_back(testpoint.head(3).norm());
				// ROS_WARN_STREAM("added lidar point for object " << j);
				// ROS_WARN_STREAM("testpoint: \n " << testpoint);
				// ROS_WARN_STREAM("testpoint.head(3).norm(): " << testpoint.head(3).norm());
			}			
		}		
	}	
	// ROS_INFO_STREAM("distances in BB found");


	// create a pointcloud to contain the reconstructed landmarks in the camera frame
	sensor_msgs::PointCloud lidar_camera_reconstructed_landmarks;
	lidar_camera_reconstructed_landmarks.header.frame_id = frame_id_camera;
	

	for(int i=0; i < object_vector.size(); ++i) {
		//create centroid point
		float u_centroid = object_vector[i].xmin + ((object_vector[i].xmax - object_vector[i].xmin)/2);
		float v_centroid = object_vector[i].ymax + ((object_vector[i].ymin - object_vector[i].ymax)/2);
		boundingbox_centroid = Eigen::Vector3f(u_centroid, v_centroid, 1.0);

		// ROS_INFO_STREAM("boundingbox_centroid: " << boundingbox_centroid);
		
		//variable for resultant reconstructed distance
		float distance;		

		//sort the distances to get the median easily
		std::sort(distances[i].begin(), distances[i].end());
	
		//find median distance <-- TODO: must be replaced with a "findMedian" function
		// ROS_INFO_STREAM("distances[" << i << "].size(): " << distances[i].size());
		if(distances[i].size() % 2 == 0 && distances[i].size() != 0){
			//median distance if even number of points
			distance = (distances[i][(distances[i].size()/2)-1] + distances[i][distances[i].size()/2])/2;
			// ROS_INFO_STREAM("distance: " << distance);

            // skip this iteration if object is too far
            if (distance > max_dist) continue; 

			// reconstruct 3D point
			reconstructed_point = K_inv*boundingbox_centroid;
			
			// normalize reconstructed point
			reconstructed_point = reconstructed_point/(reconstructed_point.norm());

			// use the median distance
			reconstructed_point = reconstructed_point*distance;
		
			// convert eigen vector to geometry_msgs::Point32
			geometry_msgs::Point32 pc_reconstructed_point;

			pc_reconstructed_point.x = reconstructed_point(0);
			pc_reconstructed_point.y = reconstructed_point(1);
			pc_reconstructed_point.z = reconstructed_point(2);
			
			// add recontructed point to point cloud            
			lidar_camera_reconstructed_landmarks.points.push_back(pc_reconstructed_point);
		} else if(!(distances[i].size() % 2 == 0)) {
			//median distance if odd number of points
			distance = (distances[i][(distances[i].size() - 1)/2]);
			// ROS_INFO_STREAM("distance: " << distance);

            // skip this iteration if object is too far
            if (distance > max_dist) continue;

			// reconstruct 3D point
			reconstructed_point = K_inv*boundingbox_centroid;
		
			// normalize reconstructed point
			reconstructed_point = reconstructed_point/(reconstructed_point.norm());

			// use the median distance
			reconstructed_point = reconstructed_point*distance;
			
			// convert eigen vector to geometry_msgs::Point32
			geometry_msgs::Point32 pc_reconstructed_point;

			pc_reconstructed_point.x = reconstructed_point(0);
			pc_reconstructed_point.y = reconstructed_point(1);
			pc_reconstructed_point.z = reconstructed_point(2);

			// add recontructed point to point cloud			
			lidar_camera_reconstructed_landmarks.points.push_back(pc_reconstructed_point);
		}

	}
	
	// ROS_WARN_STREAM("object point cloud constructed");

	if (lidar_camera_reconstructed_landmarks.points.size() == 0) {
		// return; // nothing to publish
	}

    // convert to pt2 to publish
    sensor_msgs::PointCloud2 msg_pt2_odom;
    sensor_msgs::convertPointCloudToPointCloud2(lidar_camera_reconstructed_landmarks, msg_pt2_odom);
    
	// ROS_INFO("%d object added. ", msg_pt2_odom.height * msg_pt2_odom.width);
    
	// reconstruction_pub.publish(msg_pt2_odom);
    
    // // bring points into the robot initial frame
    // sensor_msgs::PointCloud2 msg_pt2_map;
    // Eigen::Affine3d T_WR_inv = T_WR.inverse();
	// Eigen::Affine3d T_WR_test = Eigen::Affine3d::Identity();
    // pcl_ros::transformPointCloud(T_WR_test.matrix().cast<float>(), msg_pt2_odom, msg_pt2_map);
	// msg_pt2_map.header.frame_id = frame_id_map;

    sensor_msgs::PointCloud lidar_camera_reconstructed_landmarks_world_frame; // landmarks in the map frame (i.e., start frame)

	try {
        tf_listener.transformPointCloud(frame_id_map, lidar_camera_reconstructed_landmarks, lidar_camera_reconstructed_landmarks_world_frame);
    } catch (tf::TransformException &ex) {
        ROS_WARN_STREAM("Exception trying to transform point cloud: " << ex.what());
    }	
	sensor_msgs::PointCloud2 msg_pt2_map;
    sensor_msgs::convertPointCloudToPointCloud2(lidar_camera_reconstructed_landmarks_world_frame, msg_pt2_map); // convert to pt2
	
	
	//publish constellation message 
	// map_pub.publish(msg_pt2_map);


	/////////////////////////////////////////////////////////////////////////////////////////////
	// landmarks from t=0 till t=now
	

	// currently observed landmarks in map frame
	pcl::PointCloud<pcl::PointXYZ> landmarks_now_pcl;
	pcl::fromROSMsg(msg_pt2_map, landmarks_now_pcl);
	Eigen::MatrixXd landmarks_now = landmarks_now_pcl.getMatrixXfMap().topRows(DIM).cast<double>();

	uint landmark_history_count = landmark_history.cols(); // number of landmarks from t=0 to t=now
	uint landmarks_now_count = landmarks_now.cols(); // number of detections in current frame

	if (landmark_history_count==0) 
	{
		landmark_history = landmarks_now; // initialize as current landmarks
		detection_counts.resize(landmarks_now.cols(), 1); // initialize detection counts to 1 for all landmarks
		// ROS_INFO_STREAM("initiated landmark history");
	} 
	else // fuse previous observations with new ones
	{
		// ROS_WARN_STREAM("checking new and old landmarks");
		for (size_t i=0; i<landmarks_now_count; ++i) {
			Eigen::Vector3d p = landmarks_now.col(i);
			bool fused = false;
			// ROS_WARN_STREAM("p:\n" << p);

			std::vector<double> distvec;
			for (size_t j=0; j<landmark_history_count; ++j) {
				Eigen::Vector3d q = landmark_history.col(j);
				// ROS_WARN_STREAM("q:\n" << q);
				Eigen::Vector3d diff = p-q;
				double norm = diff.norm();
				distvec.emplace_back(norm);
				// ROS_WARN_STREAM("norm: " << norm);				 												
			}
			// closest existing landmark to current detection
			int min_idx = std::min_element(distvec.begin(),distvec.end()) - distvec.begin(); // index min element
			int min_dist = *std::min_element(distvec.begin(), distvec.end()); // min element

			if (min_dist < fusion_radius) // fuse points
			{
				Eigen::Vector3d q = landmark_history.col(min_idx);
				landmark_history.col(min_idx) = (p + q) / 2;
				detection_counts[min_idx] ++;
				fused = true;
				landmarks_updated = true;
			}

			if (!fused) // add point as a new landmark 
			{
				landmark_history.conservativeResize(landmark_history.rows(), landmark_history.cols()+1);
				landmark_history.col(landmark_history.cols()-1) = p;
				detection_counts.emplace_back(1); // add detect count of 1 for the new landmark
				landmarks_updated = true;
				// ROS_INFO_STREAM("added new landmark");
			} else 
			{				
				// ROS_INFO_STREAM("fused with old landmarks");
			}
		}		
	}

	// ROS_WARN_STREAM("Landmark construnction finished.");

	// ROS_WARN_STREAM("Landmarks:");
	// ROS_WARN_STREAM("\n" << landmark_history);


	/////////////////////////////////////////////////////////////////////////////////////////////
	// publish landmarks 

	// create point cloud object for landmarks that are detected > min_sightings
	pcl::PointCloud<pcl::PointXYZ> pt_cloud_pcl;
	for (size_t i=0; i < landmark_history.cols(); ++i) 
	{
		// ROS_INFO_STREAM("detection_counts[" << i << "]: " << detection_counts[i]);
		if (detection_counts[i] >= min_sightings) // landmarks that are detected > min_sightings
		{
			pcl::PointXYZ point;
			point.x = landmark_history(0,i);
			point.y = landmark_history(1,i);
			point.z = landmark_history(2,i);

			pt_cloud_pcl.push_back(point);
			// ROS_INFO_STREAM("point: " << point);
		}
	}

	// Convert to ROS PointCloud2 data type
	pcl::PCLPointCloud2 ptcloud2;
	pcl::toPCLPointCloud2(pt_cloud_pcl, ptcloud2);
	sensor_msgs::PointCloud2 msg_pt2_landmarks;	
	pcl_conversions::fromPCL(ptcloud2, msg_pt2_landmarks);
	msg_pt2_landmarks.header.frame_id = frame_id_map;
	msg_pt2_landmarks.header.stamp = detection_msg->header.stamp;

	
	// publish landmakrs
	landmarks_pub.publish(msg_pt2_landmarks);
	
	// publish only if landmarks where updated:
	// if (landmarks_updated) {
	// 	landmarks_pub.publish(msg_pt2_landmarks);
	// }

	// ROS_WARN_STREAM("landmarks published.");
}

// ----------------------------------------------------------------------------

int main(int argc, char **argv) 
{
	ros::init(argc, argv, "mapper_arl");
	ros::NodeHandle nh("");
    ros::NodeHandle nhp("~");
	Mapper mapper(nh, nhp);
	ros::spin();
	return 0;
}
