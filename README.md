# Object_Mapper 

ROS package that creates a PointCloud2 object map from bounding-box detections in the image, lidar points, and the robot's odometry (in tf-tree).
<!-- The node "mapper_sim" subscribes to the detection topics that are generated by the ARL simulator. -->
The node "mapper_arl" subscribes to the detection topics gnerated by ARL Phoeninx in the form of "arl_perception_msgs".

<!-- Demo video with 3 robots: https://youtu.be/v5jIy5QEPiY -->


## Dependencies
The package requires ROS custom messages defined in "arl_perception_msgs" and "omnimapper_msgs", which are avilable in the ARL Phoenix repository. 


## Expected topics
The node will subscribe to the following topics:
- /robot/lidar_points (lidar points as sensor_msgs::PointCloud2)
- /robot/detections (object detection topics as arl_perception_msgs::BoundingBoxArray)
- /robot/camera_info (camera_info topic, which contains the camera intrinsic matrix, as sensor_msgs::CameraInfo)

The topic "landmarks" will be generated, which is the x-y-z positions of objects in the reference frame of the robot.


## Launch file 
See "launch_gq_bag.launch" in the launch folder. 
Remap topic names & params as needed.

## Parameters
- "max_dist": maximum distance of object from camera to be considered a detected object
- "fusion_radius": will fuse landmarks that are within the fusion_radius proximity
- "min_sightings": minimum number of times that an object has to be observed to be added to map
- "frame_id_camera": name of the camera frame in tf tree
- "frame_id_map": name of the map frame (i.e., start frame) in tf tree




